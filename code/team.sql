/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : team

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-12-04 15:49:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `username` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `password` varchar(255) CHARACTER SET utf8 DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES ('houdini', '4204a67ab05f1f1c88e077b19f2befda');

-- ----------------------------
-- Table structure for check0
-- ----------------------------
DROP TABLE IF EXISTS `check0`;
CREATE TABLE `check0` (
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `year` int(255) DEFAULT NULL,
  `uploadtime` datetime DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `type` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `address` varchar(255) CHARACTER SET utf8 DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of check0
-- ----------------------------
INSERT INTO `check0` VALUES ('人机交互.rp', '2020', '2020-12-04 14:53:39', '人机交互', '资料', '../待审核文件/人机交互.rp');
INSERT INTO `check0` VALUES ('Bootstrap-v3.3.5中文api.chm', '2014', '2020-12-04 14:52:14', 'Java', '资料', '../待审核文件/Bootstrap-v3.3.5中文api.chm');

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '文件id',
  `year` int(255) DEFAULT NULL,
  `uploadtime` datetime DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `type` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `address` varchar(255) CHARACTER SET utf8 DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES ('Vue心得模板.txt', '2018', '2020-11-23 15:30:19', 'Vue', '资料', '../存储文件/Vue心得模板.txt');
INSERT INTO `file` VALUES ('练习卷(十)填空必考', '2020', '2020-11-25 22:20:34', '高等数学', '小道消息', '');
INSERT INTO `file` VALUES ('20200911-第1次课-linux虚拟机环境和oracle数据库初体验.zip', '2020', '2020-11-25 22:44:33', 'Oracle', '资料', '../存储文件/20200911-第1次课-linux虚拟机环境和oracle数据库初体验.zip');
INSERT INTO `file` VALUES ('虚拟机启动-常用linux命令-oracle初体验.mp4', '2019', '2020-11-25 22:44:33', 'Oracle', '资料', '../存储文件/虚拟机启动-常用linux命令-oracle初体验.mp4');
INSERT INTO `file` VALUES ('team.sql', '2014', '2020-12-04 15:16:41', 'Java', '资料', '../待审核文件/team.sql');
