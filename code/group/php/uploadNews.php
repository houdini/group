<?php
	include("conn.php");
  header('content-type:text/html; charset=utf-8;');
  date_default_timezone_set("Asia/Hong_kong");
  $news=$_POST['news'];
  $subject=$_POST['subject'];
  $year=date('Y');
  $date=date('Y-m-d H:i:s');
  $type="小道消息";
  $count=mb_strlen($news);

  function strPosF($content){
      $sh = file_get_contents('../shield.txt'); // 读取关键字文本信息
      $content = trim($content); 
      $shArr = explode("\n",$sh);
       // 把关键字转换为数组
      for ($i=0; $i < count($shArr) ; $i++){
         if ($shArr[$i] == "") {
              continue; //如果关键字为空就跳过本次循环
          }
          if (strpos($content,trim($shArr[$i])) !== false){
              return $shArr[$i]; //如果匹配到关键字就返回关键字
          }
      }
      return false; // 如果没有匹配到关键字就返回 false
  }

  for($i=0; $i<$count; $i++){
      $key = trim(strPosF($news));
      if ($key){
          $cot=mb_strlen($key);
          $star="";
          for($j=0;$j<$cot;$j++){
            $star=$star."*";
          }
          $news=str_replace($key,$star, $news);
      }
  }

  $stmt=$db->prepare('insert into file(name,subject,year,uploadtime,type) values(?,?,?,?,?)');
  $stmt->execute(array($news,$subject,$year,$date,$type));

  echo "<script type='text/javascript'>alert('资料上传成功，返回主页');location='../userIndex.html';</script>";

?>

