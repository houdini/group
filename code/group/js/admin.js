 $.ajax({
url:"./php/all.php",
dataType:"JSON",
success: function(data){
    var str ="<tr><th>文件名</th><th>上传时间</th><th>所属科目</th><th>所属年份</th><th>所属类型</th><th>操作</th></tr>";
        for(var k in data){
            str = str+"<tr><td><a href=\"./php/view.php?Name="+data[k].name+"\">"+data[k].name+"</a></td>";
            str = str+"<td>"+data[k].uploadtime+"</td>";
            str = str+"<td>"+data[k].subject+"</td>";
            str = str+"<td>"+data[k].year+"</td>";
            str = str+"<td>"+data[k].type+"</td>";
            str = str+"<td><button value=\""+data[k].name+"\" onclick=\"del('"+data[k].name+"')\" id=\"filename\">删除</button></td></tr>";
        }
        $("#test").html(str);
    }
});

function del(kw){
    $.ajax({
    type:'POST',
    url:"./php/del.php",
    data:{Name:kw},
    dataType:"JSON",
    success: function(data){
    var str ="<tr><th>文件名</th><th>上传时间</th><th>所属科目</th><th>所属年份</th><th>所属类型</th><th>操作</th></tr>";
        for(var k in data){
            str = str+"<tr><td><a href=\"view.php?Name="+data[k].name+"\">"+data[k].name+"</a></td>";
            str = str+"<td>"+data[k].uploadtime+"</td>";
            str = str+"<td>"+data[k].subject+"</td>";
            str = str+"<td>"+data[k].year+"</td>";
            str = str+"<td>"+data[k].type+"</td>";
            str = str+"<td><button value=\""+data[k].name+"\" onclick=\"del('"+data[k].name+"')\" id=\"filename\">删除</button></td></tr>"; 
        }
        $("#test").html(str);
    }
    });
};

function search(){
    var kw = $("#keyword").val();
    $.ajax({
        type:'POST',
        url:"./php/search.php",
        data:{keyword:kw},
        dataType:"JSON",
        success: function(data){
            var str ="<tr><th>文件名</th><th>上传时间</th><th>所属科目</th><th>所属年份</th><th>所属类型</th><th>操作</th></tr>";
            var change = 0;
            var notice ="";
            for(var k in data){
                str = str+"<tr><td><a href=\"./php/view.php?Name="+data[k].name+"\">"+data[k].name+"</a></td>";
                str = str+"<td>"+data[k].uploadtime+"</td>";
                str = str+"<td>"+data[k].subject+"</td>";
                str = str+"<td>"+data[k].year+"</td>";
                str = str+"<td>"+data[k].type+"</td>";
                str = str+"<td><button value=\""+data[k].name+"\" onclick=\"del('"+data[k].name+"')\" id=\"filename\">删除</button></td></tr>";
                change = 1;
            }
            if(change==0)
                notice = notice+"<h3>此次查询结果为空</h3>";
            $("#test").html(str);
            $("#notice").html(notice);
        }
    });
};

function sort(tt){
    var part = "";
    var msg = "";
    var type ="";
    if(tt==1){
        part="uploadtime";
        if($("#upload").val()=="按上传时间降序"){
            document.getElementById("upload").value = "按上传时间升序";
            type = document.getElementById("now").innerHTML;
        }
        else{
            document.getElementById("upload").value ="按上传时间降序";
            msg = "desc";
            type = document.getElementById("now").innerHTML;
        }
    }
    else{
        part="year";
        if($("#year").val()=="按所属年份降序"){
            document.getElementById("year").value = "按所属年份升序";
            type = document.getElementById("now").innerHTML;
        }
        else{
            document.getElementById("year").value ="按所属年份降序";
            msg = "desc";
            type = document.getElementById("now").innerHTML;
        }
    }
    var kw = $("#keyword").val();
    $.ajax({
        type:'POST',
        url:"./php/sort.php",
        data:{msg:msg,keyword:kw,part:part,type:type},
        dataType:"JSON",
        success: function(data){
            var str ="<tr><th>文件名</th><th>上传时间</th><th>所属科目</th><th>所属年份</th><th>所属类型</th><th>操作</th></tr>";
            var change = 0;
            var notice ="";
            for(var k in data)
            {
                str = str+"<tr><td><a href=\"./php/view.php?Name="+data[k].name+"\">"+data[k].name+"</a></td>";
                str = str+"<td>"+data[k].uploadtime+"</td>";
                str = str+"<td>"+data[k].subject+"</td>";
                str = str+"<td>"+data[k].year+"</td>";
                str = str+"<td>"+data[k].type+"</td>";
                str = str+"<td><button value=\""+data[k].name+"\" onclick=\"del('"+data[k].name+"')\" id=\"filename\">删除</button></td></tr>";
                change = 1;
            }
            if(change==0)
                notice = notice+"<h3>此次查询结果为空</h3>";
            $("#test").html(str);
            $("#notice").html(notice);
        }
    });
};

function show(){
    var msg = "";
    var kw = $("#keyword").val();
    var  button = $("#message").val();
    if(button=="小道消息"){
         document.getElementById("message").value="资料";
         document.getElementById("now").innerHTML="小道消息";
         msg="小道消息";

    }
    else if(button=="资料"){
         document.getElementById("message").value="所有类型";
         document.getElementById("now").innerHTML="资料";
         msg="资料";
    }
    else{
         document.getElementById("message").value="小道消息";
         document.getElementById("now").innerHTML="所有类型";
    }
    $.ajax({
        type:'POST',
        url:"./php/show.php",
        data:{msg:msg,kw:kw},
        dataType:"JSON",
        success: function(data){
            var str ="<tr><th>文件名</th><th>上传时间</th><th>所属科目</th><th>所属年份</th><th>所属类型</th><th>操作</th></tr>";
            var change = 0;
            var notice ="";
            for(var k in data){
                str = str+"<tr><td><a href=\"./php/view.php?Name="+data[k].name+"\">"+data[k].name+"</a></td>";
                str = str+"<td>"+data[k].uploadtime+"</td>";
                str = str+"<td>"+data[k].subject+"</td>";
                str = str+"<td>"+data[k].year+"</td>";
                str = str+"<td>"+data[k].type+"</td>";
                str = str+"<td><button value=\""+data[k].name+"\" onclick=\"del('"+data[k].name+"')\" id=\"filename\">删除</button></td></tr>";
                change = 1;
            }
            if(change==0)
                notice = notice+"<h3>查看关于“"+kw+"”的"+button+"操作查无</h3>";
            $("#test").html(str);
            $("#notice").html(notice);
        }
    });
};